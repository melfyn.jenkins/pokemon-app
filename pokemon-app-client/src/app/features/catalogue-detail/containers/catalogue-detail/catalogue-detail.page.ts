import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Pokemon } from "src/app/models/pokemon.model";
import { CatalogueDetailService } from "../../services/catalogue-detail.service";

@Component({
    selector: 'app-catalogue-detail-page',
    templateUrl: './catalogue-detail.page.html'

})
export class CatalogueDetailPage implements OnInit {

    private readonly pokemonName: string = '';

    constructor(private readonly route: ActivatedRoute, private readonly catalogueDetailService: CatalogueDetailService) {
        // get the parameter from pokemon/name: -> equal name in route in app-routing.module.ts
        this.pokemonName = this.route.snapshot.paramMap.get('name');
    }

    ngOnInit(): void {
        this.catalogueDetailService.fetchPokemonByName(this.pokemonName);
    }

    get pokemon(): Pokemon {
        return this.catalogueDetailService.pokemon;
    }
}