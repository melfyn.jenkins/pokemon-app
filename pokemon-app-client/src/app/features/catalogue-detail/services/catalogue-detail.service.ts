import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Pokemon } from "src/app/models/pokemon.model";
import { getMasterImageUrl } from "src/app/utils/pokemon-image.utils";
import { environment } from "src/environments/environment";

const { pokemonApi } = environment;

@Injectable({
    providedIn: 'root'
})
export class CatalogueDetailService {

    public pokemon: Pokemon;

    constructor(private readonly http: HttpClient) {

    }

    public fetchPokemonByName(name: string): void {
        this.http.get<Pokemon>(`${pokemonApi}/pokemon/${name}`)
            .pipe(
                map((pokemon: Pokemon) => ({
                    ...pokemon,
                    image: getMasterImageUrl(pokemon.id)
                }))
            )
            .subscribe((pokemon: Pokemon) => {
                this.pokemon = pokemon;
            });
    }

}