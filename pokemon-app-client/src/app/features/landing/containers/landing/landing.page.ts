import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppRoutes } from "src/app/enums/app-routes.enum";

@Component({
    selector: 'app-landing-page',
    templateUrl: './landing.page.html'

})
export class LandingPage {

    constructor(private readonly router: Router) {

    }

    handleLoginSuccess(): void {
        // Redirect
        this.router.navigateByUrl(AppRoutes.CATALOGUE);
    }
}