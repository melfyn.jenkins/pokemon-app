import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormGroup, FormControl, Validators, AbstractControl } from "@angular/forms";
import { finalize } from "rxjs/operators";
import { getStorage } from "src/app/utils/storage.utils";
import { LoginService } from "../../services/login.service";


@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html'
})
export class LoginFormComponent implements OnInit {

    @Output() success: EventEmitter<void> = new EventEmitter();

    constructor(private readonly loginService: LoginService) {

    }

    // Binding to Reactform and validating
    loginForm: FormGroup = new FormGroup({
        trainerName: new FormControl('', [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
            Validators.pattern('[a-zA-Z0-9 ]*')
        ])
    });

    ngOnInit(): void {
        const existingTrainer = getStorage<any>('pk-tr');
        if (existingTrainer !== null) {
            this.success.emit();
            /*  this.loginForm.patchValue({
                  trainerName: existingTrainer.name
              });  if you wanted to auto fill value from storage in login*/
        }
    }

    get trainerName(): AbstractControl {
        return this.loginForm.get('trainerName');
    }

    get loading(): boolean {
        return this.loginService.loading;
    }

    get error(): string {
        return this.loginService.error;
    }

    get errorCount(): number {
        return this.loginService.errorCount;
    }

    onStartClicked() {
        const { trainerName } = this.loginForm.value; // receives an object and why we destructure

        this.loginService.login(trainerName)
            .subscribe(
                this.handleSuccessfulLogin.bind(this),
                this.handleErrorLogin.bind(this)
            );
    }

    onRegisterClick() {
        const { trainerName } = this.loginForm.value;
        this.loginService.register(trainerName)
            .subscribe((response) => {
                this.success.emit();
            });
    }

    handleSuccessfulLogin(trainer): void {
        this.success.emit();
    }

    handleErrorLogin({ message = 'An error occured' }): void {
        if (this.loginService.errorCount >= 3) {

        }
        console.log(this.error);
    }
}