import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import { catchError, finalize, map, mergeMap, tap } from 'rxjs/operators';
import { setStorage } from "src/app/utils/storage.utils";
import { StorageKeys } from "src/app/enums/storage-keys.enum";
import { Trainer } from "src/app/models/trainer.model";

const { trainerApi } = environment;


@Injectable({
    providedIn: 'root'
})
export class LoginService {

    public loading: boolean = false;
    public error: string = '';
    public errorCount: number = 0;

    constructor(private readonly http: HttpClient) {

    }

    private setTrainer(trainer: Trainer): void {
        setStorage<Trainer>(StorageKeys.TRAINER, trainer);
    }

    register(name: string): Observable<any> {
        return this.http.post(`${trainerApi}/trainers`, {
            name,
            pokemon: []
        }).pipe(
            map((trainer: Trainer) => {
                this.setTrainer(trainer);
                return trainer;
            })
        )
    }

    // RXJS -> chain operators in the pipe
    login(trainerName: string): Observable<any> {
        this.loading = true;

        // mergeMap -> allows us to merge two observables
        // of -> creates an observable from an Object

        const login$ = this.http.get<Trainer[]>(`${trainerApi}/trainers?name=${trainerName}`)
        const register$ = this.http.post(`${trainerApi}/trainers`, {
            name: trainerName,
            pokemon: []
        })

        return login$.pipe(
            mergeMap((loginResponse: Trainer[]) => {
                // wait for login$ to complete
                const trainer = loginResponse.pop() as Trainer;
                delete trainer.id; // delete id before storing in localstorage
                if (!trainer) {
                    return register$;
                } else {
                    return of(trainer);
                }
            }),
            tap((trainer: Trainer) => {
                this.setTrainer(trainer);
            }),
            finalize(() => {
                this.loading = false;
            })
        );
    }

}