import { Component, OnInit } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { CatalogueService } from "../../services/catalogue.service";

@Component({
    selector: 'app-catalogue-page',
    templateUrl: './catalogue.page.html'

})
export class CataloguePage implements OnInit {

    constructor(private readonly catalogueService: CatalogueService) {
    }

    ngOnInit(): void {
        this.catalogueService.fetchPokemons();
    }

    get pokemon(): Pokemon[] {
        return this.catalogueService.pokemon;
    }

}