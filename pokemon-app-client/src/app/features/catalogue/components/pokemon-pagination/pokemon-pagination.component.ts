import { Component } from "@angular/core";
import { CatalogueService } from "../../services/catalogue.service";

@Component({
    selector: 'app-pokemon-pagination',
    templateUrl: './pokemon-pagination.component.html'
})
export class PokemonPaginationComponent {

    constructor(private readonly catalogueService: CatalogueService) {

    }

    get isFirstPage(): boolean {
        return this.catalogueService.isFirstPage;
    }

    get isLastPage(): boolean {
        return this.catalogueService.isLastPage;
    }

    onPreviousClick(): void {
        this.catalogueService.previous();
    }

    onNextClick(): void {
        this.catalogueService.next();
    }


}