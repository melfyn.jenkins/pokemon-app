import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";
import { PokemonResponse } from "src/app/models/pokemon-response.model";
import { Pokemon } from "src/app/models/pokemon.model";
import { getMasterImageUrl } from "src/app/utils/pokemon-image.utils";
import { environment } from "src/environments/environment";

const { pokemonApi } = environment
@Injectable({
    providedIn: 'root'
})
export class CatalogueService {

    private readonly pokemonCache$;
    private _pokemon: Pokemon[] = [];
    error: string = '';

    private offset = 0;
    private limit = 20;
    public count = 0;
    public isLastPage: boolean = false;
    public isFirstPage: boolean = true;

    constructor(private readonly http: HttpClient) {
        this.pokemonCache$ = this.http.get<PokemonResponse>(`${pokemonApi}/pokemon?limit=151`)
            .pipe(shareReplay(1)) // shares replay everytime you to fetch pokemons in fetchpokemon function below
    }

    get pokemon(): Pokemon[] {

        return this._pokemon.slice(
            //  this.paginator.offsetStart,
            //this.paginator.offsetEnd
        );
    }

    public next(): void {
        const nextOffset = this.offset + this.limit;
        if (nextOffset <= this.count - 1) {
            this.offset += this.limit;
        }

        this.updatePageStatus();
    }

    public previous(): void {
        if (this.offset !== 0) {
            this.offset -= this.limit;
        }

        this.updatePageStatus();
    }

    private updatePageStatus(): void {
        this.isFirstPage = this.offset === 0;
        this.isLastPage = this.offset === this.count - this.limit;
    }

    // api receives next, count results -> only interested in results
    fetchPokemons(): void {
        this.pokemonCache$
            .pipe(
                map((response: PokemonResponse) => {
                    return response.results.map((pokemon: Pokemon) => ({
                        ...pokemon,
                        ...this.getIdAndImage(pokemon.url)
                    }));
                })
            )
            .subscribe(
                (pokemon: Pokemon[]) => {
                    this._pokemon = pokemon;
                    this.count = pokemon.length;
                },
                (error: HttpErrorResponse) => {
                    this.error = error.message;
                }
            );
    }

    private getIdAndImage(url: string): any {
        const id = Number(url.split('/').filter(Boolean).pop());
        return {
            id,
            image: getMasterImageUrl(id)
        };
    }
}