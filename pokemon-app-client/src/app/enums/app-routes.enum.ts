export enum AppRoutes {
    LOGIN = '',
    CATALOGUE = '/catalogue',
    CATALOGUE_DETAIL = '/catalogue',
    TRAINER = '/trainer'
}