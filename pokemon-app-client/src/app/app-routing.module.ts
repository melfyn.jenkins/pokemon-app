import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CatalogueDetailPage } from "./features/catalogue-detail/containers/catalogue-detail/catalogue-detail.page";
import { CataloguePage } from "./features/catalogue/containers/catalogue/catalogue.page";
import { LandingPage } from "./features/landing/containers/landing/landing.page";
import { TrainerPage } from "./features/trainer/containers/trainer/trainer.page";
import { SessionGuard } from "./guards/session.guard";

// Define Application Routes:
const routes: Routes = [

    {
        path: '',
        component: LandingPage
    },
    {
        path: 'catalogue',
        component: CataloguePage,
        canActivate: [SessionGuard]
    },
    {
        path: 'catalogue/:name',
        component: CatalogueDetailPage,
        canActivate: [SessionGuard]
    },
    {
        path: 'trainer',
        component: TrainerPage,
        canActivate: [SessionGuard]
    }
]


@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}