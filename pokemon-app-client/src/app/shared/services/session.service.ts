import { Injectable } from "@angular/core";
import { StorageKeys } from "src/app/enums/storage-keys.enum";
import { getStorage } from "src/app/utils/storage.utils";

@Injectable({
    providedIn: 'root'

})
export class SessionService {

    // checks if a session is stored
    active(): boolean {
        const trainer = getStorage(StorageKeys.TRAINER);

        return Boolean(trainer);
    }

}