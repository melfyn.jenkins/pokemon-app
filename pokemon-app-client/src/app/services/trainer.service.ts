import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class TrainerService {

    constructor(private readonly http: HttpClient) {

    }
    // check if trainer exists
    // ! -> create the user.
    fetchTrainer(name: string) {
        this.http.get(`${environment.trainerApi}/trainers?name=${name}`).subscribe(response => {
            console.log(response)
        })
    }


    // create a trainer
    createTrainer(name: string) {
        // 1. CHANGE THIS TO A POST request.
        // 2. { name: 'Some name', pokemon: [] }
        this.http.get(`${environment.trainerApi}/trainers`).subscribe(response => {
            console.log(response)
        })
    }


    // store trainer

    // maybe, store locally. 

}