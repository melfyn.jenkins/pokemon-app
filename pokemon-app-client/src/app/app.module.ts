import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LandingPage } from './features/landing/containers/landing/landing.page';
import { TrainerPage } from './features/trainer/containers/trainer/trainer.page';
import { CataloguePage } from './features/catalogue/containers/catalogue/catalogue.page';
import { CatalogueDetailPage } from './features/catalogue-detail/containers/catalogue-detail/catalogue-detail.page';
import { LoginFormComponent } from './features/landing/components/login-form/login-form.component';
import { BaseButtonComponent } from './shared/components/base-button/base-button.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { PokemonGridComponent } from './features/catalogue/components/pokemon-grid/pokemon-grid.component';
import { AppContainerComponent } from './shared/container/container.component';
import { PokemonProfileHeaderComponent } from './features/catalogue-detail/components/pokemon-profile-header/pokemon-profile-header.component';
import { PokemonPaginationComponent } from './features/catalogue/components/pokemon-pagination/pokemon-pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    TrainerPage,
    CataloguePage,
    CatalogueDetailPage,
    // Component
    LoginFormComponent,
    BaseButtonComponent,
    NavbarComponent,
    PokemonGridComponent,
    AppContainerComponent,
    PokemonProfileHeaderComponent,
    PokemonPaginationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
