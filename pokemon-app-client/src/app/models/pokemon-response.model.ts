import { Pokemon } from "./pokemon.model";
// model for api response
export interface PokemonResponse {
    count: number;
    next: string;
    prev: string;
    results: Pokemon[];

}