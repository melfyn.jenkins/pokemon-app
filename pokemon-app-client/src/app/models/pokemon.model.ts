export interface Pokemon {
    id?: number;
    name: string;
    url: string;
    image?: string;

    weight?: number;
    height?: number;
    types?: PokemonType[];
    stats?: PokemonStat[];
    sprites?: any[];
}

export interface PokemonSprite {
    front_shiny: string;
    back_shiny: string;
    other: PokemonSpriteOther;
}

export interface PokemonSpriteOther {
    dream_world: any;
    'official-artwork': PokemonSpriteOfficial;
}

export interface PokemonSpriteOfficial {
    front_default: string;
}

export interface PokemonType {
    slot: number;
    type: PokemonTypeType;
}

export interface PokemonTypeType {
    name: string;
    url: string;
}

export interface PokemonStat {
    base_stat: number;
    effort: number;
    stat: PokemonStatStat;
}

export interface PokemonStatStat {
    name: string;
    url: string;
}