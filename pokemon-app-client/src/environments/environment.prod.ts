export const environment = {
  production: true,
  pokemonApi: 'https://pokeapi.co/api/v2',
  trainerApi: 'http://pokemon-trainer.herokuapp.com/'
};
