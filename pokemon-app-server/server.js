const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()
const { PORT = 5000 } = process.env
server.use(middlewares)
server.use(router)

server.listen(PORT, () => console.log(`Server started on port ${PORT}`));